@extends('layouts.app')

@section('content')
    <div class="container">

        <h2>
            Import Profile(s) from Asana as an University Membership
        </h2>

        @include("importProfileAsanaUniMbr::import.profile.asana.uni-mbr.form.import")

    </div>
@endsection
