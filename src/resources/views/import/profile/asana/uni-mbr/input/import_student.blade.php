<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_student"
                name="import_student"
                aria-label="Checkbox for importing as Students"
                disabled
            >
        </div>
    </div>
    <label class="input-group-text" for="import_student">Import as Student</label>
</div>
