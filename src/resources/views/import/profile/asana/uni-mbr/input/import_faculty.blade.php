<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_faculty"
                name="import_faculty"
                aria-label="Checkbox for importing as Faculty"
            >
        </div>
    </div>
    <label class="input-group-text" for="import_faculty">Import as Faculty</label>
</div>
