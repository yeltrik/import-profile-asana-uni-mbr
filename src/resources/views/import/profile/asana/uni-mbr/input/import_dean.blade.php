<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_dean"
                name="import_dean"
                aria-label="Checkbox for importing as Deans"
            >
        </div>
    </div>
    <label class="input-group-text" for="import_dean">Import as Dean</label>
</div>
