<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_department_head"
                name="import_department_head"
                aria-label="Checkbox for importing as Department Heads"
            >
        </div>
    </div>
    <label class="input-group-text" for="import_department_head">Import as Department Head</label>
</div>
