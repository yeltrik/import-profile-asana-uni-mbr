<div class="input-group mb-3">
    <div class="input-group-prepend">
        <div class="input-group-text">
            <input
                type="checkbox"
                id="import_staff"
                name="import_staff"
                aria-label="Checkbox for importing as Staff"
            >
        </div>
    </div>
    <label class="input-group-text" for="import_staff">Import as Staff</label>
</div>
