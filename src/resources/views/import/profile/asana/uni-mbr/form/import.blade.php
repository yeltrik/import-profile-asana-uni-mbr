<form
    method="POST"
    action="{{ action([\Yeltrik\ImportProfileAsanaUniMbr\app\http\controllers\ImportProfileAsanaUniMbrController::class, 'store']) }}"
    enctype="multipart/form-data"
>
    @csrf

    <hr>
    <h3>
        Profile
    </h3>
    @include('importProfileAsana::import.profile.asana.input.all')

    <hr>

    <h3>
        University Membership
    </h3>
    @include('importProfileAsanaUniMbr::import.profile.asana.uni-mbr.input.group.uni_mbr')

    <br>
    @include('importProfileAsana::import.profile.asana.input.import_button')

</form>
