<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueContraintToProfileUniMbrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('import_profile_asana_uni_mbr')->table('profile_uni_mbr', function (Blueprint $table) {
            $table->unique(['profile_id', 'member_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('import_profile_asana_uni_mbr')->table('profile_uni_mbr', function (Blueprint $table) {
            $table->dropUnique(['profile_id', 'member_id']);
        });
    }
}
