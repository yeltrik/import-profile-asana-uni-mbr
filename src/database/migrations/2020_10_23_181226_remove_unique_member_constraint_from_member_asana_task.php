<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveUniqueMemberConstraintFromMemberAsanaTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('import_profile_asana_uni_mbr')->table('member_asana_task', function (Blueprint $table) {
            $table->dropUnique(['member_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('import_profile_asana_uni_mbr')->table('member_asana_task', function (Blueprint $table) {
            $table->unique(['member_id']);
        });
    }
}
