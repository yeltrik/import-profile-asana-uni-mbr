<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportProfileAsanaUniMbr\app\http\controllers\ImportProfileAsanaUniMbrController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/profile/asana/uni-mbr/create',
    [ImportProfileAsanaUniMbrController::class, 'create'])
    ->name('imports.profiles.asana.uni-mbr.create');

Route::post('import/profile/asana/uni-mbr',
    [ImportProfileAsanaUniMbrController::class, 'store'])
    ->name('imports.profiles.asana.uni-mbr.store');
