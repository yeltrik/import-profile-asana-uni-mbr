<?php

namespace Yeltrik\ImportProfileAsanaUniMbr\app;

use Yeltrik\ImportProfileAsanaUniMbr\app\import\Abstract_AsanaMemberRowImporter;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\MemberAsanaTask;
use Yeltrik\UniMbr\app\models\Member;

class AsanaUniMemberCreator extends Abstract_AsanaMemberRowImporter
{

    /**
     * @param Member $member
     */
    private function createMemberAsanaTask(Member $member)
    {
        $gid = $this->row()['Task ID'];
        $memberAsanaTask = new MemberAsanaTask();
        $memberAsanaTask->member()->associate($member);
        $memberAsanaTask->asana_gid = $gid;
        $memberAsanaTask->save();
    }

    /**
     * @return Member
     */
    public function process(): Member
    {
        if (static::rowIsNew($this->row())) {
            if ( $this->memberIsNew() ) {
                $member = new Member();
            } else {
                $member = $this->getMember();
            }

            if ($member instanceof Member) {
                $this->processMemberAttributes($member);
                $this->createMemberAsanaTask($member);
            }
            return $member;
        } else {
            dd('not new');
        }
    }

}
