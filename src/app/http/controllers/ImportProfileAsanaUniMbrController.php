<?php

namespace Yeltrik\ImportProfileAsanaUniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Yeltrik\ImportProfileAsanaUniMbr\app\import\AsanaProfileUniMbrCsvImporter;

class ImportProfileAsanaUniMbrController extends Controller
{

    /**
     * ImportProfileAsanaUniMbrController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function create()
    {
        return view('importProfileAsanaUniMbr::import.profile.asana.uni-mbr.create');
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        switch ($request->import_method) {
            case "export_csv":
                AsanaProfileUniMbrCsvImporter::validate($request);
                $importer = new AsanaProfileUniMbrCsvImporter($request);
                return $importer->process();
                break;
            default:
                dd('Unsupported Import Method: ' . $request->import_method);
                break;
        }
    }
}
