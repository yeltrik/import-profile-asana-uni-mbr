<?php

namespace Yeltrik\ImportProfileAsanaUniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\UniMbr\app\models\Member;

/**
 * Class MemberAsanaTask
 *
 * @property int id
 * @property int member_id
 * @property string asana_gid
 *
 * @property Member member
 *
 * @package Yeltrik\ImportProfileAsanaUniMbr\app\models
 */
class MemberAsanaTask extends Model
{
    use HasFactory;

    protected $connection = "import_profile_asana_uni_mbr";
    public $table = "member_asana_task";

    /**
     * @return BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id', 'id');
    }

}
