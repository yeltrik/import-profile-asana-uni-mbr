<?php

namespace Yeltrik\ImportProfileAsanaUniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniMbr\app\models\Member;

/**
 * Class ProfileUniMbr
 *
 * @property int id
 * @property int profile_id
 * @property int member_id
 *
 * @property Profile profile
 * @property Member member
 *
 * @package App\Models
 */
class ProfileUniMbr extends Model
{
    use HasFactory;

    protected $connection = "import_profile_asana_uni_mbr";
    public $table = "profile_uni_mbr";

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    /**
     * @return BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

}
