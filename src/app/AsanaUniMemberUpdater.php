<?php


namespace Yeltrik\ImportProfileAsanaUniMbr\app;


use Yeltrik\ImportProfileAsanaUniMbr\app\import\Abstract_AsanaMemberRowImporter;
use Yeltrik\UniMbr\app\models\Member;

class AsanaUniMemberUpdater extends Abstract_AsanaMemberRowImporter
{

    /**
     * @return Member
     */
    public function process(): Member
    {
        if (static::rowExists($this->row())) {
            $member = $this->getMemberFromRow();
            if ($member instanceof Member) {
                $this->processMemberAttributes($member);
            }
            return $member;
        }
    }

}
