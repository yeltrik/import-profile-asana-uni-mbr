<?php


namespace Yeltrik\ImportProfileAsanaUniMbr\app\import;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaCsvRow;
use Yeltrik\ImportProfileAsanaUniMbr\app\AsanaUniMemberUpdater;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\MemberAsanaTask;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniMbr\app\models\Member;

abstract class Abstract_AsanaMemberRowImporter extends Abstract_ImportAsanaCsvRow
{

    /**
     * @return Builder|Model|object|null
     */
    public function getMember()
    {
        $query = $this->memberQuery();
        if ($query instanceof Builder) {
            return $query->first();
        } else {
            return NULL;
        }
    }

    /**
     * @return HigherOrderBuilderProxy|mixed|Member|null
     */
    protected function getMemberFromRow()
    {
        $gid = $this->row()['Task ID'];
        $memberAsanaTask = MemberAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->first();
        if ($memberAsanaTask instanceof MemberAsanaTask) {
            return $memberAsanaTask->member;
        } else {
            return NULL;
        }
    }

    /**
     * @return mixed|null
     */
    public function getRowEmail()
    {
        if (isset($this->row()['Email'])) {
            $email = trim($this->row()['Email']);
            if ($email != NULL) {
                return $email;
            } else {
                return NULL;
            }
        }
    }

    /**
     * @return |null
     */
    public function getRowLogin()
    {
        return NULL;
    }

    /**
     * @return string|null
     */
    public function getRowName()
    {
        if (isset($this->row()['Name'])) {
            $name = trim($this->row()['Name']);
            if ($name != NULL) {
                return $name;
            }
        }

        if (isset($this->row()['First Name']) && isset($this->row()['Last Name'])) {
            $first = trim($this->row()['First Name']);
            $last = trim($this->row()['Last Name']);
            if ($first != NULL && $last != NULL) {
                $name = "$first $last";
                return $name;
            }
        }

        return NULL;
    }

    /**
     * @return |null
     */
    public function getRowNumber()
    {
        return NULL;
    }

    /**
     * @return bool|null
     */
    public function memberIsNew()
    {
        $query = $this->memberQuery();
        if ($query instanceof Builder) {
            return !$query->exists();
        } else {
            return TRUE;
        }
    }

    /**
     * @return Builder|null
     */
    public function memberQuery()
    {
        $query = Member::query();

        if ($this->getRowEmail() != NULL) {
            $query->orWhere('email', '=', $this->getRowEmail());
        }

        if ($this->getRowLogin() != NULL) {
            $query->orWhere('login', '=', $this->getRowLogin());
        }

        if ($this->getRowName() != NULL) {
            $query->orWhere('name', '=', $this->getRowName());
        }

        if ($this->getRowNumber() != NULL) {
            $query->orWhere('number', '=', $this->getRowNumber());
        }

        if (!empty($query->getQuery()->wheres)) {
            return $query;
        } else {
            return NULL;
        }
    }

    /**
     * @param Member $member
     */
    public function processMemberAttributes(Member $member)
    {
        if ($member instanceof Member) {
            $this->processMemberEmail($member);
            //$this->processMemberLogin($member);
            $this->processMemberName($member);
            //$this->processMemberNumber($member);
            $member->save();
        }
    }

    /**
     * @param Member $member
     */
    public function processMemberEmail(Member &$member)
    {
        if (isset($this->request()['import_email'])) {
            $emailStr = trim(strtolower($this->row()['Email']));
            if ($emailStr == NULL) {
                $member->email = NULL;
            } else {
                $member->email = $emailStr;
            }
        }
    }

    /**
     * @param Member $member
     */
    public function processMemberName(Member &$member)
    {
        if (array_key_exists('Name', $this->row())) {
            $member->name = $this->row()['Name'];
        } else {
            // Attempt to get first and last from Name (Delimited by "," or " ")
            $first = trim($this->row()['First Name']);
            $last = trim($this->row()['Last Name']);
            if ( $first != NULL && $last != NULL ) {
                $member->name = "$first $last";
            }
        }
    }

    /**
     * @param array $task
     * @return bool
     */
    public static function rowExists(array $task)
    {
        $gid = $task['Task ID'];
        return MemberAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->exists();
    }

    /**
     * @param array $row
     * @return bool
     */
    public static function rowIsNew(array $row)
    {
        return !AsanaUniMemberUpdater::rowExists($row);
    }

}
