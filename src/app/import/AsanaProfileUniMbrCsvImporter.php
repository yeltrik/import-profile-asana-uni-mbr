<?php

namespace Yeltrik\ImportProfileAsanaUniMbr\app\import;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\ImportProfileAsana\app\Abstract_AsanaProfileImporter;
use Yeltrik\ImportProfileAsanaUniMbr\app\AsanaUniMemberCreator;
use Yeltrik\ImportProfileAsanaUniMbr\app\AsanaUniMemberUpdater;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\Profile\app\import\ProfileImporter;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniMbr\app\models\Dean;
use Yeltrik\UniMbr\app\models\Member;

class AsanaProfileUniMbrCsvImporter extends Abstract_AsanaProfileImporter
{

    /**
     * @return RedirectResponse
     */
    public function process()
    {
        foreach ($this->csv() as $row) {
            $this->processRow($row);
        }

        return back()
            ->with('success', 'You have successfully upload file.');
    }

    /**
     * @param $row
     */
    private function processAttributes($row)
    {
        $asanaMemberAttributeImporter = new AsanaMemberAttributeImporter($this->request(), $row);
        $asanaMemberAttributeImporter->process();
    }

    /**
     * @param array $row
     */
    private function processRow(array $row)
    {
        if ($this->shouldProcessProject($row)) {
            if ($this->request()->import_member === "on") {

                if (AsanaUniMemberCreator::rowIsNew($row)) {
                    $asanaUniMemberCreator = new AsanaUniMemberCreator($this->request(), $row);
                    $member = $asanaUniMemberCreator->process();
                    $this->processAttributes($row);
                } else {
                    $asanaUniMemberUpdater = new AsanaUniMemberUpdater($this->request(), $row);
                    $member = $asanaUniMemberUpdater->process();
                    $this->processAttributes($row);
                }

                $profileImporter = new ProfileImporter();
                $email = $row['Email'];
                $profileImporter->associateWithEmailStr($email);
                $firstName = $row['First Name'];
                $lastName = $row['Last Name'];
                $profileImporter->associateWithPersonalNameStr($firstName, $lastName);
                $profile = $profileImporter->profile();

                if ( $profile instanceof Profile ) {
                    // Check if Member and Profile Exists
                    $profileUniMbrQuery = ProfileUniMbr::query()
                        ->where('profile_id', '=', $profile->id)
                        ->where('member_id', '=', $member->id);
                    if (!$profileUniMbrQuery->exists()) {
                        // Join Member and Profile
                        $profileUniMbr = new ProfileUniMbr();
                        $profileUniMbr->profile()->associate($profile);
                        $profileUniMbr->member()->associate($member);
                        $profileUniMbr->save();
                    }
                } else {
                    //dd($row);
                }
            }
        }
    }

    /**
     * @param array $row
     * @return bool
     */
    private function shouldProcessProject(array $row): bool
    {
        $asanaProject = $this->request()['asana_project'];
        if (isset($row['Projects'])) {
            $projectsStr = $row['Projects'];
            $projects = array_map('trim', explode(',', $projectsStr));
            return in_array($asanaProject, $projects);
        } else {
            //ROW is Corrupt?
            //dd($row);
            return false;
        }
    }

    /**
     * @param Request $request
     * @return array|void
     */
    public static function validate(Request $request)
    {
        // Validate only if Option was to Upload
        $request->validate([
            'asana_export_csv_file' => 'required|mimes:txt|max:20480',
            'asana_project' => 'required',
            'import_member' => 'required'
        ]);
    }

}
