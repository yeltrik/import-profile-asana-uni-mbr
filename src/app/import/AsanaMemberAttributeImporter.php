<?php


namespace Yeltrik\ImportProfileAsanaUniMbr\app\import;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniMbr\app\models\Dean;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniMbr\app\models\Rank;
use Yeltrik\UniMbr\app\models\Staff;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;

class AsanaMemberAttributeImporter extends Abstract_AsanaMemberRowImporter
{
    /**
     * @return Builder|Model|object|null
     */
    public function getCollege()
    {
        return College::query()->where('name', '=', $this->getCollegeName())->first();
    }

    /**
     * @return mixed
     */
    public function getCollegeName()
    {
        return $this->row()['College'];
    }

    /**
     * @return Builder|Model|object|null
     */
    public function getDepartment()
    {
        return Department::query()->where('name', '=', $this->getDepartmentName())->first();
    }

    /**
     * @return mixed
     */
    public function getDepartmentName()
    {
        return $this->row()['Department'];
    }

    public function process()
    {
        $this->processDean();
        $this->processDepartmentHead();
        $this->processFaculty();
        $this->processStaff();
//        $this->processStudent();
    }

    /**
     *
     */
    public function processDean()
    {
        if ($this->request()['import_dean'] && $this->getCollege() instanceof College) {
            $member = $this->getMemberFromRow();
            if ($member instanceof Member) {
                if ($member->dean()->exists()) {
                    $dean = $member->dean;
                } else {
                    $dean = new Dean();
                    $dean->member()->associate($member);
                }
                $dean->college()->associate($this->getCollege());
                $dean->save();
            } else {
                dd('member not available');
            }
        }
    }

    /**
     *
     */
    public function processDepartmentHead()
    {
        if ($this->request()['import_department_head'] && $this->getDepartment() instanceof Department) {
            $member = $this->getMemberFromRow();
            if ($member instanceof Member) {
                if ($member->departmentHead()->exists()) {
                    $departmentHead = $member->departmentHead;
                } else {
                    $departmentHead = new DepartmentHead();
                    $departmentHead->member()->associate($member);
                }
                $departmentHead->department()->associate($this->getDepartment());
                $departmentHead->save();
            } else {
                dd('member not available');
            }
        }
    }

    /**
     *
     */
    public function processFaculty()
    {
        if ($this->request()['import_faculty'] && $this->getDepartment() instanceof Department) {
            $member = $this->getMemberFromRow();
            if ($member instanceof Member) {
                if ( $member->faculty()->exists() ) {
                    $faculty = $member->faculty;
                } else {
                    $faculty = new Faculty();
                    $faculty->member()->associate($member);
                }
                $faculty->department()->associate($this->getDepartment());

                // TODO: This is Creating Ranks that are NULL
                $facultyRank = $this->row()['Faculty Rank'];
                $rank = Rank::query()->where('name', '=', $facultyRank)->first();
                if ($rank instanceof Rank == FALSE ) {
                    $rank = new Rank();
                    $rank->name = $facultyRank;
                    $rank->save();
                }
                $faculty->rank()->associate($rank);

                $faculty->save();
            } else {
                dd('member not available');
            }
        }
    }

    /**
     *
     */
    public function processStaff()
    {
        if ($this->request()['import_staff'] && $this->getDepartment() instanceof Department) {
            $member = $this->getMemberFromRow();
            if ($member instanceof Member) {
                if ( $member->staff()->exists() ) {
                    $staff = $member->staff;
                } else {
                    $staff = new Staff();
                    $staff->member()->associate($member);
                }
                $staff->department()->associate($this->getDepartment());
                $staff->save();
            } else {
                dd('member not available');
            }
        }
    }

}
